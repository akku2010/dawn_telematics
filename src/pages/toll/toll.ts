import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

/**
 * Generated class for the TollPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-toll',
  templateUrl: 'toll.html',
})
export class TollPage {
  list: any[];
  public toggled: boolean = false;

  public toggle(): void {
    this.toggled = !this.toggled;
  }
  cancelSearch() {
    // this.toggle();
    this.toggled = false;
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public apicalldaywise: ApiServiceProvider) {
    this.toggled = false;
    this.tollList();
  }

  callSearch(ev) {
    var searchKey = ev.target.value;
    var _baseURL;

      //_baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        _baseURL = "https://www.oneqlik.in/toll/get?search=" + searchKey

    this.apicalldaywise.callSearchService(_baseURL)
      .subscribe(data => {
        // this.apiCall.stopLoading();
        console.log("search result=> " + JSON.stringify(data))
        this.list = data;
        //this.allDevices = data.devices;
        // console.log("fuel percentage: " + data.devices[0].fuel_percent)
      },
        err => {
          console.log(err);
          // this.apiCall.stopLoading();
        });
  }

  onClear(ev) {
    this.tollList();
    // this.getdevicesTemp();
    ev.target.value = '';
    // this.toggled = false;
  }

  live() {
    this.navCtrl.push("TollMapPage")
  }

  tollList() {
    console.log("inside", 123);
    var baseURLp = "https://www.oneqlik.in/toll/get"
    this.apicalldaywise.startLoading().present();
    this.apicalldaywise.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalldaywise.stopLoading();
        console.log("toll list", data);
        this.list = data;
      },
        err => {
          this.apicalldaywise.stopLoading();
          console.log(err);
        });
  }

  addTolls() {
    this.navCtrl.push('TollAddTollPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TollPage');
  }

}
